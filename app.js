const express = require('express');
const app = express(); 

/**
 * Add to number together
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number} sum of a and b
 */

const add = (a,b) => {
    return a + b;
}

app.get('',(req, res) => {
    const sun = add(1,2);
    console.log(sun);
    res.send('ok')
});

app.listen(3000, () => {
    console.log("Server listening to localhost:3000");
});